# Bing Wallpaper

Grabs the latest Bing wallpaper and displays it.
## Compiling
Just run:

```bash
sudo cp ./bin/bbg /usr/local/bin/bbg
sudo chmod +x /usr/local/bin/bbg
```
